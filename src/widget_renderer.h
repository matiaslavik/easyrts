#pragma once
#include "asset_manager.h"
#include "image_visual.h"
#include "text_visual.h"
#include "widget_render_params.h"
#include "widget.h"

namespace EasyRTS
{
   class WidgetRenderer
   {
   private:
      AssetManager* mAssetManager;
      Widget* mRootWidget;
      sf::RenderWindow* mRenderWindow;

      void renderImageVisual(ImageVisual* imageVisual, const WidgetRenderParams renderParams);
      void renderTextVisual(TextVisual* textVisual, const WidgetRenderParams renderParams);

      void renderWidget(Widget* widget, WidgetRenderParams renderParams);

   public:
      WidgetRenderer(sf::RenderWindow* renderWindow, AssetManager* assetManager);

      void renderWidgets();
      void addWidget(Widget* widget);
   };
}
