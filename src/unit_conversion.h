#pragma once

#include "glm/vec2.hpp"

namespace EasyRTS
{
   class UnitConversion
   {
   public:
      static inline glm::vec2 cartesianToIsometric(glm::vec2 pos)
      {
         return { pos.x - pos.y, (pos.x + pos.y) * 0.5f };
      }

      static inline glm::vec2 isometricToCartesian(glm::vec2 pos)
      {
         return { (2.0f * pos.y + pos.x) * 0.5f, (2.0f * pos.y - pos.x) * 0.5f };
      }
   };
}
