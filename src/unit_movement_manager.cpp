#include "unit_movement_manager.h"
#include "scene.h"
#include "path_query.h"
#include "glm/detail/func_geometric.hpp"
#include <algorithm>
#include "glm/gtx/vector_angle.hpp"

namespace EasyRTS
{
   UnitMovementManager::UnitMovementManager(Scene* scene)
   {
      mScene = scene;
   }

   void UnitMovementManager::addUnitMovement(std::weak_ptr<Unit> unit, std::shared_ptr<PathQuery> pathQuery)
   {
      UnitMovementHandle handle;
      handle.mUnit = unit;
      handle.mPathQuery = pathQuery;
      handle.mNeedsAnimUpdate = true;
      mUnits[unit.lock()->mGUID] = handle;
   }

   void UnitMovementManager::updateUnits(float deltaTime)
   {
      if (deltaTime > 0.5f)
         deltaTime = 0.5f; // TODO: update in steps?

      std::vector<std::shared_ptr<Unit>>& untis = mScene->getUnits();

      // Remove deleted units
      for (auto it = mUnits.begin(); it != mUnits.end();)
      {
         if (it->second.mUnit.expired() || (it->second.mPathQuery->mResultReady && it->second.mPathQuery->mPath.empty()))
            it = mUnits.erase(it);
         else
            ++it;
      }
      
      // Update units
      std::for_each(/*std::execution::par, */mUnits.begin(), mUnits.end(), [&](std::pair<const uint64_t, UnitMovementHandle>& pair)
         {
            UnitMovementHandle& handle = pair.second;
            std::shared_ptr<PathQuery> pathQuery = handle.mPathQuery;
            std::shared_ptr<Unit> unit = handle.mUnit.lock();
            glm::vec2 dir(0.0f, 0.0f);
            bool hasDir = false;

            if (pathQuery->mResultReady)
            {
               while (!hasDir && !pathQuery->mPath.empty())
               {
                  glm::vec2 currTarget = glm::vec2(pathQuery->mPath.top()) + glm::vec2(0.5f, 0.5f);
                  dir = currTarget - unit->mCartesianPos;
                  if (glm::length(dir) < 0.1f)
                  {
                     pathQuery->mPath.pop();
                     handle.mNeedsAnimUpdate = true;
                  }
                  else
                     hasDir = true;
               }
            }
            else
            {
               // TODO: Move in direction of target?
            }

            if (hasDir)
            {
               unit->setPosition(unit->mCartesianPos + glm::normalize(dir) * deltaTime * 2.0f);
               if (handle.mNeedsAnimUpdate)
                  unit->setMoveAnim(dir);
            }
            else if(handle.mNeedsAnimUpdate)
               unit->setIdleAnim();

            handle.mNeedsAnimUpdate = false;
         });
   }
}
