#pragma once

#include <chrono>

namespace EasyRTS
{
   class TimeManager
   {
   private:
      std::chrono::time_point<std::chrono::steady_clock>  mStartTimePoint;
      std::chrono::time_point<std::chrono::steady_clock>  mCurrentTimePoint;
      double mTime = 0.0f;
      double mDeltaTime = 0.0f;

   public:
      void initialise();
      void updateTime();
      float getTimeSeconds();
      float getDeltaTimeSeconds();
   };
}

