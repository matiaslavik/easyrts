#pragma once
#include "visual.h"
#include "SFML/Graphics.hpp"

namespace EasyRTS
{
   class ImageVisual : public Visual
   {
   public:
      ImageVisual();
      ~ImageVisual();

      sf::Sprite* mSprite = nullptr;
      std::string mImagePath;

      virtual VisualType getType() override { return VisualType::ImageVisual; }

   };
}
