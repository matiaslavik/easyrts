#pragma once
#include "visual.h"
#include "SFML/Graphics.hpp"
#include <string>
#include "glm/vec2.hpp"

namespace EasyRTS
{
   class TextVisual : public Visual
   {
   private:
      void updatePosition();

   public:
      TextVisual();
      ~TextVisual();

      sf::Text mText;
      sf::Font mFont;
      glm::vec2 mOrigin;

      virtual VisualType getType() override { return VisualType::TextVisual; }

      void setText(const std::string text);
      void setOrigin(glm::vec2 origin);
   };
}
