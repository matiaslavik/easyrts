#include "entity_ref.h"

namespace EasyRTS
{
   EntityRef::EntityRef(std::shared_ptr<Building> building)
   {
      mEntity = std::static_pointer_cast<Entity>(building);
      mEntityType = EntityType::Building;
   }

   EntityRef::EntityRef(std::shared_ptr<Resource> resource)
   {
      mEntity = std::static_pointer_cast<Entity>(resource);
      mEntityType = EntityType::Resource;
   }

   EntityRef::EntityRef(std::shared_ptr<Unit> unit)
   {
      mEntity = std::static_pointer_cast<Entity>(unit);
      mEntityType = EntityType::Unit;
   }

   EntityRef::EntityRef(const EntityRef& other)
   {
      mEntity = other.mEntity;
      mEntityType = other.mEntityType;
   }

   EntityRef::EntityRef(std::nullptr_t ptr)
   {
      mEntity = nullptr;
      mEntityType = EntityType::Invalid;
   }

   EntityRef::EntityRef()
   {
      mEntity = nullptr;
      mEntityType = EntityType::Invalid;
   }

   bool EntityRef::isBuilding() const
   {
      return mEntityType == EntityType::Building;
   }

   bool EntityRef::isResource() const
   {
      return mEntityType == EntityType::Resource;
   }

   bool EntityRef::isUnit() const
   {
      return mEntityType == EntityType::Unit;
   }

   bool EntityRef::isValid() const
   {
      return mEntity != nullptr;
   }

   std::shared_ptr<Entity> EntityRef::getEntity()
   {
      return mEntity;
   }

   std::shared_ptr<Building> EntityRef::getBuilding()
   {
      if (mEntityType == EntityType::Building)
         return std::static_pointer_cast<Building>(mEntity);
      else
         return nullptr;
   }

   std::shared_ptr<Resource> EntityRef::getResource()
   {
      if (mEntityType == EntityType::Resource)
         return std::static_pointer_cast<Resource>(mEntity);
      else
         return nullptr;
   }

   std::shared_ptr<Unit> EntityRef::getUnit()
   {
      if (mEntityType == EntityType::Unit)
         return std::static_pointer_cast<Unit>(mEntity);
      else
         return nullptr;
   }
}
