#pragma once
#include "building.h"
#include "unit.h"
#include "resource.h"
#include <memory>

namespace EasyRTS
{
   enum class EntityType
   {
      Invalid,
      Building,
      Resource,
      Unit
   };

   class EntityRef
   {
   private:
      EntityType mEntityType;
      std::shared_ptr<Entity> mEntity;

   public:
      EntityRef(std::shared_ptr<Building> building);
      EntityRef(std::shared_ptr<Resource> resource);
      EntityRef(std::shared_ptr<Unit> unit);
      EntityRef(const EntityRef& other);
      EntityRef(std::nullptr_t ptr);
      EntityRef();
      ~EntityRef() {}

      bool isBuilding() const;
      bool isResource() const;
      bool isUnit() const;
      bool isValid() const;

      std::shared_ptr<Entity> getEntity();
      std::shared_ptr<Building> getBuilding();
      std::shared_ptr<Resource> getResource();
      std::shared_ptr<Unit> getUnit();
   };
}
