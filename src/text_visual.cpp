#include "text_visual.h"
#include <cassert>

namespace EasyRTS
{
   TextVisual::TextVisual()
   {
      bool fontLoaded = mFont.loadFromFile("assets/FreeSans.ttf"); // TODO
      assert(fontLoaded);
      mText.setFont(mFont);
      mText.setCharacterSize(24);
      mText.setFillColor(sf::Color::Black);
   }

   TextVisual::~TextVisual()
   {
      
   }

   void TextVisual::updatePosition()
   {
      sf::FloatRect textRect = mText.getLocalBounds();
      mText.setOrigin(textRect.left + textRect.width * mOrigin.x, textRect.top + textRect.height * mOrigin.y);
   }

   void TextVisual::setText(const std::string text)
   {
      mText.setString(text);

      updatePosition();
   }

   void TextVisual::setOrigin(glm::vec2 origin)
   {
      mOrigin = origin;
      updatePosition();
   }
}
