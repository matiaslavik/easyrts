#pragma once

#include <string>
#include <glm/vec2.hpp>
#include "spritesheet_info.h"

namespace EasyRTS
{
   class UnitInfo
   {
   public:
      std::string mUnitName;
      int mHealth;
      SpritesheetInfo* mSpritesheet;
      std::string mThumbnail;
   };
}
