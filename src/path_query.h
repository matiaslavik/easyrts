#pragma once
#include <glm/vec2.hpp>
#include <atomic>
#include <stack>

namespace EasyRTS
{
   class PathQuery
   {
   public:
      glm::ivec2 mStartTile;
      glm::ivec2 mTargetTile;
      std::atomic<bool> mResultReady;
      std::stack<glm::ivec2> mPath;
   };
}
