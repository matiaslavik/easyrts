#pragma once

#include "widget_transform.h"

namespace EasyRTS
{
	class WidgetRenderParams
	{
	public:
		/**
		* Defines the position and extent of the content to be rendered.
		* This is decided by the transform of the widget.
		*/
		WidgetRect mContentRect;

		/**
		* Defines visible region of the content to be rendered.
		* This is decided by the transform of the parent widget.
		*/
		WidgetRect mVisibleRect;

		/**
		* True, if the transform changed or a Visual was added this frame.
		*/
		bool mVisualsNeedUpdate;
	};
}
