#pragma once
#include <unordered_map>
#include <memory>
#include <cstdint>
#include "glm/vec2.hpp"

namespace EasyRTS
{
   class Scene;
   class Unit;
   class PathQuery;

   struct UnitMovementHandle
   {
      std::weak_ptr<Unit> mUnit;
      std::shared_ptr<PathQuery> mPathQuery;
      bool mNeedsAnimUpdate = false;
   };

   class UnitMovementManager
   {
   private:
      Scene* mScene;
      std::unordered_map<uint64_t, UnitMovementHandle> mUnits;

   public:
      UnitMovementManager(Scene* scene);

      void addUnitMovement(std::weak_ptr<Unit> unit, std::shared_ptr<PathQuery> pathQuery);
      void updateUnits(float deltaTime);
   };
}
