#pragma once
#include <vector>
#include "building.h"
#include "resource.h"
#include "unit.h"
#include <set>
#include <memory>
#include "entity_ref.h"

namespace sf
{
   class Texture;
   class Sprite;
}

namespace EasyRTS
{
   class AssetManager;

   class Scene
   {
   private:
      std::vector<EntityRef> mEntities;
      std::vector<std::shared_ptr<Unit>> mUnits;
      sf::Texture* mTileTexture;
      std::vector<sf::Sprite> mMapSprites; // TODO: optimise
      std::vector<glm::ivec2> mGroundTiles;
      glm::ivec2 mMapDimension;
      AssetManager* mAssetManager;

      void crateGroundSprites();

   public:
      Scene(AssetManager* assetManager);

      std::shared_ptr<Building> addBuilding(BuildingInfo* buildingInfo);
      std::shared_ptr<Resource> addResource(ResourceInfo* resourceInfo);
      std::shared_ptr<Unit> addUnit(UnitInfo* unitInfo);

      void setTilemap(sf::Texture* texture);
      void setMapSize(const int width, const int height);
      void setGroundTile(const int tileX, const int tileY, const int spriteX, const int spriteY);

      void tickScene(float deltaTime);

      EntityRef getEntityAtPosition(const float x, const float y);

      std::vector<sf::Sprite>& getGroundSprites() { return mMapSprites; };

      std::vector<EntityRef>& getEntities() { return mEntities; }
      std::vector<std::shared_ptr<Unit>>& getUnits() { return mUnits; }

      glm::ivec2 getMapSize() { return mMapDimension; }
   };
}
