#include "script_manager.h"
#include <iostream>
#include "game_engine.h"
#include "asset_manager.h"
#include "building.h"
#include "unit.h"
#include "resource.h"
#include <fstream>
#include "chaiscript/chaiscript.hpp"
#include "image_widget.h"
#include "text_widget.h"
#include "widget_renderer.h"
#include <cstdlib>
#include "entity_ref.h"
#include "input_manager.h"
#include "path_query_manager.h"
#include "unit_movement_manager.h"

namespace EasyRTS
{
   ScriptManager::ScriptManager(GameEngine* engine)
   {
      mGameEngine = engine;

      addScriptBindings();
   }

   void ScriptManager::registerScript(const std::string scriptPath)
   {
      if (mChaiScript != nullptr && mRegisteredScripts.find(scriptPath) == mRegisteredScripts.end())
      {
         std::ifstream fileStream(scriptPath);
         if (!fileStream.is_open())
         {
            std::cout << "Did not find script file: " << scriptPath << std::endl;
            return;
         }
         std::string strScript((std::istreambuf_iterator<char>(fileStream)), std::istreambuf_iterator<char>());

         // UTF? => remove first 3 bytes
         if (strScript.compare(0, 3, "\xEF\xBB\xBF") == 0)
         {
            strScript.erase(0, 3);
         }

         try
         {
            mChaiScript->eval(strScript);
         }
         catch (std::exception ex)
         {
            std::cout << "Failed to evaluate script: " << scriptPath << " - " << ex.what() << std::endl;
            return;
         }
         mRegisteredScripts.insert(scriptPath);
      }
   }

   void ScriptManager::invokeStartupHandler()
   {
      registerScript("assets/GameStartupHandler.chai"); // TODO: Put path in config file

      try
      {
         mGameStartupHandler = new chaiscript::Boxed_Value(mChaiScript->eval("GameStartupHandler()"));
         std::function<void(chaiscript::Boxed_Value&)> funcStartGame = mChaiScript->eval<std::function<void(chaiscript::Boxed_Value&)>>("startGame");
         funcStartGame(*mGameStartupHandler);
      }
      catch (std::exception ex)
      {
         std::cout << "Failed to invoke GameStartupHandler:  " << ex.what() << std::endl;
         return;
      }
   }

   void ScriptManager::invokeUpdate(float deltaTime)
   {
      try
      {
         std::function<void(chaiscript::Boxed_Value&, float)> funcUpdate = mChaiScript->eval<std::function<void(chaiscript::Boxed_Value&, float)>>("update");
         funcUpdate(*mGameStartupHandler, deltaTime);
      }
      catch (std::exception ex)
      {
         std::cout << "Failed to invoke GameStartupHandler.update:  " << ex.what() << std::endl;
         return;
      }
   }

   void ScriptManager::addScriptBindings()
   {
      // Enums
      chaiscript::ModulePtr module = chaiscript::ModulePtr(new chaiscript::Module());
      chaiscript::utility::add_class<WidgetPositioningMode>(*module,
         "WidgetPositioningMode",
         { { WidgetPositioningMode::Absolute, "AbsoluteWidgetPositioning" },
         {	WidgetPositioningMode::Relative, "RelativeWidgetPositioning" }
         }
      );
      chaiscript::utility::add_class<WidgetScalingMode>(*module,
         "WidgetPositioningMode",
         { { WidgetScalingMode::Absolute, "AbsoluteWidgetScaling" },
         {	WidgetScalingMode::Relative, "RelativeWidgetScaling" }
         }
      );

      module->add(chaiscript::user_type<GameEngine>(), "GameEngine");
      module->add(chaiscript::user_type<AssetManager>(), "AssetManager");
      module->add(chaiscript::fun(&AssetManager::getTexture), "getTexture");

      module->add(chaiscript::user_type<Scene>(), "Scene");
      module->add(chaiscript::fun(&Scene::setTilemap), "setTilemap");
      module->add(chaiscript::fun(&Scene::setMapSize), "setMapSize");
      module->add(chaiscript::fun(&Scene::setGroundTile), "setGroundTile");
      module->add(chaiscript::fun(&Scene::getEntityAtPosition), "getEntityAtPosition");

      // Entities
      module->add(chaiscript::user_type<Entity>(), "Entity");
      module->add(chaiscript::fun(&Entity::setPosition), "setPosition");
      module->add(chaiscript::user_type<Unit>(), "Unit");
      module->add(chaiscript::base_class<Entity, Unit>());
      module->add(chaiscript::user_type<Building>(), "Building");
      module->add(chaiscript::base_class<Entity, Building>());
      module->add(chaiscript::user_type<Resource>(), "Resource");
      module->add(chaiscript::base_class<Entity, Resource>());

      module->add(chaiscript::user_type<EntityRef>(), "EntityRef");
      module->add(chaiscript::constructor<EntityRef()>(), "EntityRef");
      module->add(chaiscript::constructor<EntityRef(std::nullptr_t)>(), "EntityRef");
      module->add(chaiscript::fun([](EntityRef& a, EntityRef& b) { a = b; }), "=");
      module->add(chaiscript::fun(&EntityRef::isBuilding), "isBuilding");
      module->add(chaiscript::fun(&EntityRef::isResource), "isResource");
      module->add(chaiscript::fun(&EntityRef::isUnit), "isUnit");
      module->add(chaiscript::fun(&EntityRef::getEntity), "getEntity");
      module->add(chaiscript::fun(&EntityRef::getBuilding), "getBuilding");
      module->add(chaiscript::fun(&EntityRef::getResource), "getResource");
      module->add(chaiscript::fun(&EntityRef::getUnit), "getUnit");
      module->add(chaiscript::fun(&EntityRef::isValid), "isValid");

      module->add(chaiscript::user_type<sf::Texture>(), "Texture");

      // glm::vec2
      module->add(chaiscript::user_type<glm::vec2>(), "vec2");
      module->add(chaiscript::constructor<glm::vec2(float, float)>(), "vec2");
      module->add(chaiscript::constructor<glm::vec2(const glm::vec2&)>(), "vec2");
      module->add(chaiscript::constructor<glm::vec2(const glm::ivec2&)>(), "vec2");
      module->add(chaiscript::constructor<glm::vec2()>(), "vec2");
      module->add(chaiscript::fun(&glm::vec2::x), "x");
      module->add(chaiscript::fun(&glm::vec2::y), "y");
      module->add(chaiscript::fun([](glm::vec2 a, glm::vec2 b) { return a + b; }), "+");
      module->add(chaiscript::fun([](glm::vec2 a, glm::vec2 b) { return a - b; }), "-");
      module->add(chaiscript::fun([](glm::vec2 a, glm::vec2 b) { return a * b; }), "*");
      module->add(chaiscript::fun([](glm::vec2 a, glm::vec2 b) { return a / b; }), "/");
      module->add(chaiscript::fun([](glm::vec2 a, float b) { return a * b; }), "*");
      module->add(chaiscript::fun([](glm::vec2& a, const glm::vec2& b) { a = b; }), "=");
      module->add(chaiscript::fun([](glm::vec2& a, const glm::vec2& b) { a *= b; }), "*=");
      module->add(chaiscript::fun([](glm::vec2& a, const glm::vec2& b) { a /= b; }), "/=");
      module->add(chaiscript::fun([](glm::vec2& a, const glm::vec2& b) { a += b; }), "+=");
      module->add(chaiscript::fun([](glm::vec2& a, const glm::vec2& b) { a -= b; }), "-=");
      module->add(chaiscript::fun([](glm::vec2& a, float b) { a *= b; }), "*=");
      module->add(chaiscript::fun([](glm::vec2& a, float b) { a /= b; }), "/=");

      module->add(chaiscript::user_type<glm::ivec2>(), "ivec2");
      module->add(chaiscript::constructor<glm::ivec2(int, int)>(), "ivec2");
      module->add(chaiscript::constructor<glm::ivec2(const glm::vec2&)>(), "ivec2");
      module->add(chaiscript::constructor<glm::ivec2()>(), "ivec2");
      module->add(chaiscript::fun(&glm::ivec2::x), "x");
      module->add(chaiscript::fun(&glm::ivec2::y), "y");
      module->add(chaiscript::fun([](glm::ivec2 a, glm::ivec2 b) { return a + b; }), "+");
      module->add(chaiscript::fun([](glm::ivec2 a, glm::ivec2 b) { return a - b; }), "-");
      module->add(chaiscript::fun([](glm::ivec2 a, glm::ivec2 b) { return a * b; }), "*");
      module->add(chaiscript::fun([](glm::ivec2 a, glm::ivec2 b) { return a / b; }), "/");
      module->add(chaiscript::fun([](glm::ivec2 a, int b) { return a * b; }), "*");
      module->add(chaiscript::fun([](glm::ivec2& a, const glm::ivec2& b) { a = b; }), "=");
      module->add(chaiscript::fun([](glm::ivec2& a, const glm::ivec2& b) { a *= b; }), "*=");
      module->add(chaiscript::fun([](glm::ivec2& a, const glm::ivec2& b) { a /= b; }), "/=");
      module->add(chaiscript::fun([](glm::ivec2& a, const glm::ivec2& b) { a += b; }), "+=");
      module->add(chaiscript::fun([](glm::ivec2& a, const glm::ivec2& b) { a -= b; }), "-=");
      module->add(chaiscript::fun([](glm::ivec2& a, int b) { a *= b; }), "*=");
      module->add(chaiscript::fun([](glm::ivec2& a, int b) { a /= b; }), "/=");

      module->add(chaiscript::fun([&]() { return mGameEngine->getAssetManager(); }), "getAssetManager");

      // Widgets
      module->add(chaiscript::user_type<Widget>(), "Widget");
      module->add(chaiscript::fun([&](Widget& widget, glm::vec2 pos) { widget.setPosition(pos); } ), "setPosition");
      module->add(chaiscript::fun([&](Widget& widget, glm::vec2 pivot) { widget.setPivot(pivot); } ), "setPivot");
      module->add(chaiscript::fun([&](Widget& widget, glm::vec2 size) { widget.setSize(size); }), "setSize");
      module->add(chaiscript::fun(&Widget::addWidget), "addWidget");
      module->add(chaiscript::fun(&Widget::setHorizontalPositioning), "setHorizontalPositioning");
      module->add(chaiscript::fun(&Widget::setVerticalPositioning), "setVerticalPositioning");
      module->add(chaiscript::fun(&Widget::setHorizontalScaling), "setHorizontalScaling");
      module->add(chaiscript::fun(&Widget::setVerticalScaling), "setVerticalScaling");

      module->add(chaiscript::user_type<ImageWidget>(), "ImageWidget");
      module->add(chaiscript::base_class<Widget, ImageWidget>());
      module->add(chaiscript::fun(&ImageWidget::setImagePath), "setImagePath");

      module->add(chaiscript::user_type<TextWidget>(), "TextWidget");
      module->add(chaiscript::base_class<Widget, TextWidget>());
      module->add(chaiscript::fun(&TextWidget::setText), "setText");
      module->add(chaiscript::fun(&TextWidget::setOrigin), "setOrigin");

      module->add(chaiscript::fun([&](Widget* widget) { mGameEngine->getWidgetRenderer()->addWidget(widget); }), "addWidget");

      // Input manager
      module->add(chaiscript::fun([&](const int mouseButtonID) { return mGameEngine->getInputManager()->getMouseButton(mouseButtonID); }), "getMouseButton");
      module->add(chaiscript::fun([&](const int mouseButtonID) { return mGameEngine->getInputManager()->getMouseButtonDown(mouseButtonID); }), "getMouseButtonDown");
      module->add(chaiscript::fun([&](const int mouseButtonID) { return mGameEngine->getInputManager()->getMouseButtonUp(mouseButtonID); }), "getMouseButtonUp");

      module->add(chaiscript::fun([&]()
         {
         glm::vec2 pos = mGameEngine->screenToWorld(mGameEngine->getInputManager()->getMousePosition());
         return pos;
         }), "getMousePosition");

      module->add(chaiscript::fun([]() {
         return new ImageWidget();
         }), "createImageWidget");

      module->add(chaiscript::fun([]() {
         return new TextWidget();
         }), "createTextWidget");

      // Entity creation
      module->add(chaiscript::fun([&](const std::string assetPath) {
         return mGameEngine->getScene()->addBuilding(mGameEngine->getAssetManager()->getBuildingInfo(assetPath));
         }), "createBuilding");
      module->add(chaiscript::fun([&](const std::string assetPath) {
         return mGameEngine->getScene()->addUnit(mGameEngine->getAssetManager()->getUnitInfo(assetPath));
         }), "createUnit");
      module->add(chaiscript::fun([&](const std::string assetPath) {
         return mGameEngine->getScene()->addResource(mGameEngine->getAssetManager()->getResourceInfo(assetPath));
         }), "createResource");
         
      // Entity extension
      module->add(chaiscript::fun([&](Entity& entity, const std::string animation) {
         entity.mSpriteAnimator->playAnimation(animation);
         }), "playAnimation");

      module->add(chaiscript::fun([&](std::shared_ptr<Unit> unit, const glm::vec2 targetPos) {
         std::shared_ptr<PathQuery> query = std::make_shared<PathQuery>();
         query->mStartTile = glm::ivec2(unit->mCartesianPos);
         query->mTargetTile = glm::ivec2(targetPos);
         mGameEngine->getPathQueryManager()->findPath(query);
         mGameEngine->getUnitMovementManager()->addUnitMovement(unit, query);
         }), "moveTo");


      module->add(chaiscript::fun([&]() {
         return mGameEngine->getScene();
         }), "getScene");

      module->add(chaiscript::fun([&]() {
         return rand();
         }), "rand");

      mChaiScript = new chaiscript::ChaiScript();
      mChaiScript->add(module);

   }
}
