#include "scene_renderer.h"
#include <queue>

namespace EasyRTS
{
   struct EntityPositionCompare
   {
      bool operator()(const std::shared_ptr<Entity>& a, const std::shared_ptr<Entity>& b)
      {
         const float yDiff = a->mCartesianPos.y - b->mCartesianPos.y;
         const float xDiff = a->mCartesianPos.x - b->mCartesianPos.x;
         return yDiff > 0.0f || (yDiff == 0.0f && xDiff > 0.0f);
      }
   };

   SceneRenderer::SceneRenderer(Scene* scene, sf::RenderWindow* renderWindow)
   {
      mScene = scene;
      mRenderWindow = renderWindow;
   }

   void SceneRenderer::renderScene(float deltaTime)
   {
      std::vector<sf::Sprite>& mapSprites = mScene->getGroundSprites();

      // Draw map
      for (sf::Sprite& sprite : mapSprites)
      {
         mRenderWindow->draw(sprite);
      }

      // Calculate view rect
      const sf::View& view = mRenderWindow->getView();;
      sf::Vector2i viewCenter(view.getCenter());
      sf::Vector2i viewSize(view.getSize());
      sf::FloatRect currentViewRect(viewCenter.x - viewSize.x / 2, viewCenter.y - viewSize.y / 2, viewSize.x, viewSize.y);

      // Sort entities by Y and X position
      std::priority_queue<std::shared_ptr<Entity>, std::vector<std::shared_ptr<Entity>>, EntityPositionCompare> entityRenderQueue;
      for(EntityRef entityRef : mScene->getEntities())
      {
         auto entity = entityRef.getEntity();
         if(entity->mGlobalBounds.intersects(currentViewRect))
            entityRenderQueue.push(entity);
      }

      // Draw entities
      while(!entityRenderQueue.empty())
      {
         std::shared_ptr<Entity> entity = entityRenderQueue.top();
         entityRenderQueue.pop();
         entity->mSpriteAnimator->tick(deltaTime);
         glm::vec2 unitPos = entity->mIsometricPos;
         mRenderWindow->draw(*entity->mSprite);
      }
   }
}
