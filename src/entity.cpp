#include "entity.h"
#include "unit_conversion.h"

namespace EasyRTS
{
   uint64_t Entity::GUID = 0;

   const int TILE_SIZE = 32; // TODO: move to config file?
   const int TILE_ISO_WIDTH = 64; // TODO: move to config file?
   const int TILE_ISO_HEIGHT = 32; // TODO: move to config file?

   Entity::Entity()
   {
      mGUID = GUID++;
   }

   Entity::~Entity()
   {

   }

   void Entity::setPosition(glm::vec2 pos)
   {
      mCartesianPos = pos;

      // Find "root tile" (bottom centre tile) and calculate sprite offset
      glm::ivec2 rootTile = glm::ivec2(mSpritesheet->mSpriteSize.x / 2, mSpritesheet->mSpriteSize.y);
      glm::vec2 cartOffset = rootTile - glm::ivec2(TILE_ISO_WIDTH / 2, TILE_ISO_HEIGHT);

      mIsometricPos = UnitConversion::cartesianToIsometric((pos) * (float)TILE_SIZE) - cartOffset;
      
      mSprite->setPosition(mIsometricPos.x, mIsometricPos.y);
      
      mGlobalBounds = mSprite->getGlobalBounds();
   }
}
