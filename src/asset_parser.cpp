#include "asset_parser.h"
#include <fstream>
#include <iostream>

namespace EasyRTS
{
   BuildingInfo* AssetParser::parseBuilding(std::string path)
   {
      std::ifstream ifs(path);
      std::string json((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
      
      picojson::value docValue;
      std::string err = picojson::parse(docValue, json);
      if (!err.empty())
      {
         std::cout << err << std::endl;
         return nullptr;
      }

      BuildingInfo* buildingInfo = new BuildingInfo();

      const picojson::value::object& docObj = docValue.get<picojson::object>();
      for (picojson::value::object::const_iterator i = docObj.begin(); i != docObj.end(); i++)
      {
         std::string key = i->first;
         picojson::value value = i->second;

         if (key == "id")
         {
            buildingInfo->mBuildingName = value.get<std::string>();
         }
         else if (key == "health")
         {
            buildingInfo->mHealth = static_cast<int>(value.get<double>());
         }
         else if (key == "spritesheet")
         {
            buildingInfo->mSpritesheet = parseSpritesheet(value);
         }
      }

      return buildingInfo;
   }

   UnitInfo* AssetParser::parseUnit(std::string path)
   {
      std::ifstream ifs(path);
      std::string json((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));

      picojson::value docValue;
      std::string err = picojson::parse(docValue, json);
      if (!err.empty())
      {
         std::cout << err << std::endl;
         return nullptr;
      }

      UnitInfo* unitInfo = new UnitInfo();

      const picojson::value::object& docObj = docValue.get<picojson::object>();
      for (picojson::value::object::const_iterator i = docObj.begin(); i != docObj.end(); i++)
      {
         std::string key = i->first;
         picojson::value value = i->second;

         if (key == "id")
         {
            unitInfo->mUnitName = value.get<std::string>();
         }
         else if (key == "health")
         {
            unitInfo->mHealth = static_cast<int>(value.get<double>());
         }
         else if (key == "spritesheet")
         {
            unitInfo->mSpritesheet = parseSpritesheet(value);
         }
      }

      return unitInfo;
   }

   ResourceInfo* AssetParser::parseResource(std::string path)
   {
      std::ifstream ifs(path);
      std::string json((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));

      picojson::value docValue;
      std::string err = picojson::parse(docValue, json);
      if (!err.empty())
      {
         std::cout << err << std::endl;
         return nullptr;
      }

      ResourceInfo* resourceInfo = new ResourceInfo();

      const picojson::value::object& docObj = docValue.get<picojson::object>();
      for (picojson::value::object::const_iterator i = docObj.begin(); i != docObj.end(); i++)
      {
         std::string key = i->first;
         picojson::value value = i->second;

         if (key == "id")
         {
            resourceInfo->mResourceName = value.get<std::string>();
         }
         else if (key == "amount")
         {
            resourceInfo->mAmount = static_cast<int>(value.get<double>());
         }
         else if (key == "spritesheet")
         {
            resourceInfo->mSpritesheet = parseSpritesheet(value);
         }
      }

      return resourceInfo;
   }

   SpritesheetInfo* AssetParser::parseSpritesheet(picojson::value outerValue)
   {
      const picojson::value::object& object = outerValue.get<picojson::object>();

      SpritesheetInfo* spritesheetInfo = new SpritesheetInfo();

      for (picojson::value::object::const_iterator i = object.begin(); i != object.end(); i++)
      {
         std::string key = i->first;
         picojson::value value = i->second;

         if (key == "file_path")
         {
            spritesheetInfo->mSpriteSheet = value.get<std::string>();
         }
         else if (key == "sprite_size")
         {
            spritesheetInfo->mSpriteSize = parseVec2(value);
         }
         else if (key == "occupy_tiles")
         {
            spritesheetInfo->mOccupyTileCount = parseVec2(value);
         }
         else if (key == "animations")
         {
            parseAnimations(value, spritesheetInfo);
         }
      }

      return spritesheetInfo;
   }

   void AssetParser::parseAnimations(picojson::value outerValue, SpritesheetInfo* spritesheetInfo)
   {
      const picojson::value::array& array = outerValue.get<picojson::array>();

      for (picojson::value objval : array)
      {
         const picojson::value::object& arrObj = objval.get<picojson::object>();

         SpriteAnimInfo animInfo;
         std::string animID;

         for (picojson::value::object::const_iterator i = arrObj.begin(); i != arrObj.end(); i++)
         {
            std::string key = i->first;
            picojson::value value = i->second;

            if (key == "id")
            {
               animID = value.get<std::string>();
            }
            else if (key == "fps")
            {
               animInfo.mFPS = static_cast<int>(value.get<double>());
            }
            else if (key == "frames")
            {
               animInfo.mFrameCount = static_cast<int>(value.get<double>());
            }
            else if (key == "position")
            {
               animInfo.mSpritesheetPos = parseVec2(value);
            }
         }
         spritesheetInfo->mAnimations.emplace(animID, animInfo);
      }
   }

   glm::vec2 AssetParser::parseVec2(picojson::value value)
   {
      const picojson::value::object& object = value.get<picojson::object>();

      glm::vec2 vec;

      for (picojson::value::object::const_iterator i = object.begin(); i != object.end(); i++)
      {
         std::string key = i->first;
         picojson::value value = i->second;

         if (key == "x")
            vec.x = static_cast<float>(value.get<double>());
         else if (key == "y")
            vec.y = static_cast<float>(value.get<double>());
      }

      return vec;
   }
}
