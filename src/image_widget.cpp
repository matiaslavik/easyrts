#include "image_widget.h"

namespace EasyRTS
{
   ImageWidget::ImageWidget()
   {
      mImageVisual = new ImageVisual();
      addVisual(mImageVisual);
   }

   ImageWidget::~ImageWidget()
   {
      delete mImageVisual;
   }

   void ImageWidget::setImagePath(const std::string imagePath)
   {
      mImageVisual->mImagePath = imagePath;
   }
}
