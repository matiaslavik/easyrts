#include "resource.h"
#include "unit_conversion.h"

namespace EasyRTS
{
   Resource::Resource(ResourceInfo* resourceInfo, AssetManager* assetManager)
   {
      mResourceInfo = resourceInfo;

      sf::Texture* texture = assetManager->getTexture(resourceInfo->mSpritesheet->mSpriteSheet);
      mSprite = new sf::Sprite();
      mSprite->setTexture(*texture);

      mSpritesheet = resourceInfo->mSpritesheet;
      mSpriteAnimator = new SpriteAnimator(resourceInfo->mSpritesheet, mSprite);
      if (mSpriteAnimator->hasAnimation("default"))
         mSpriteAnimator->playAnimation("default"); // TEMP TEST
   }

   void Resource::setPosition(glm::vec2 pos)
   {
      Entity::setPosition(glm::ivec2(pos));
   }
}
