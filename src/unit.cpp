#include "unit.h"
#include "glm/gtx/vector_angle.hpp"

namespace EasyRTS
{
   Unit::Unit(UnitInfo* unitInfo, AssetManager* assetManager)
   {
      mUnitInfo = unitInfo;

      sf::Texture* texture = assetManager->getTexture(unitInfo->mSpritesheet->mSpriteSheet);
      mSprite = new sf::Sprite();
      mSprite->setTexture(*texture);

      mSpritesheet = unitInfo->mSpritesheet;
      mSpriteAnimator = new SpriteAnimator(unitInfo->mSpritesheet, mSprite);
      setIdleAnim();
   }

   void Unit::setMoveAnim(const glm::vec2 moveDir)
   {
      std::string moveAnim = "";

      const std::string animNames[] = {"move-NN", "move-NE", "move-EE", "move-SE" , "move-SS" , "move-SW" , "move-WW" , "move-NW" };

      const float twopi = 2.0f * 3.141592654f;

      float angle = glm::orientedAngle(glm::normalize(glm::vec2(-1.0f, -1.0f)), glm::normalize(moveDir));
      if (angle < 0)
         angle += twopi;

      int iAnim = static_cast<int>(angle * 8.0f / twopi + 0.5f) % 8;

      moveAnim = animNames[iAnim];

      mSpriteAnimator->playAnimation(moveAnim);
   }

   void Unit::setIdleAnim()
   {
      mSpriteAnimator->playAnimation("idle");
   }
}
