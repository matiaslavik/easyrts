#include "building.h"
#include "unit_conversion.h"

namespace EasyRTS
{
   Building::Building(BuildingInfo* buildingInfo, AssetManager* assetManager)
   {
      mBuildingInfo = buildingInfo;

      sf::Texture* texture = assetManager->getTexture(mBuildingInfo->mSpritesheet->mSpriteSheet);
      mSprite = new sf::Sprite();
      mSprite->setTexture(*texture);

      mSpritesheet = mBuildingInfo->mSpritesheet;
      mSpriteAnimator = new SpriteAnimator(mBuildingInfo->mSpritesheet, mSprite);
      if(mSpriteAnimator->hasAnimation("default"))
         mSpriteAnimator->playAnimation("default");
   }

   void Building::setPosition(glm::vec2 pos)
   {
      Entity::setPosition(glm::ivec2(pos));
   }
}
