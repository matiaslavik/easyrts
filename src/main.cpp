#include <iostream>
#include "game_engine.h"

int main()
{
   EasyRTS::GameEngine engine;
   engine.initialise();
   while (true)
      engine.update();

   return 0;
}
