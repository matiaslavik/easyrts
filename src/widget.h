#pragma once
#include "widget_transform.h"
#include <vector>
#include "visual.h"

namespace EasyRTS
{
   /**
   * Base class for add Widgets.
   */
   class Widget
   {
      friend class WidgetRenderer;

   protected:
      /* The local transform of this widget. Is relative to parent Widget. */
      WidgetTransform mTransform;

      /* The absolute (screen space) rect of this Widget's transform. */
      WidgetRect mAbsoluteRect;

      Widget* mParentWidget = nullptr;

      std::vector<Widget*> mChildWidgets;
      std::vector<Visual*> mVisuals;

      bool mTransformIsDirty = true;

      bool mVisualsNeedUpdate = true;

      /**
      * Marks the absolute transform as dirty.
      * This is called when doing transformations on the widget.
      */
      void setTransformDirty();

      void addVisual(Visual* visual);

   public:
      Widget();
      virtual ~Widget();

      void addWidget(Widget* widget);

      /*
      * Gets the absolute (screen space) rect of this Widget's transform.
      **/
      WidgetRect getAbsoluteRect();

      void setPosition(glm::vec2 arg_pos);
      void setSize(glm::vec2 arg_size);
      void setPivot(glm::vec2 arg_pivot);

      inline void setPosition(const float& x, const float& y) { setPosition(glm::vec2(x, y)); }
      inline void setSize(const float& w, const float& h) { setSize(glm::vec2(w, h)); }
      inline void setPivot(const float& x, const float& y) { setPivot(glm::vec2(x, y)); }

      void setVerticalPositioning(WidgetPositioningMode arg_mode);
      void setHorizontalPositioning(WidgetPositioningMode arg_mode);
      void setVerticalScaling(WidgetScalingMode arg_mode);
      void setHorizontalScaling(WidgetScalingMode arg_mode);
   };
}
