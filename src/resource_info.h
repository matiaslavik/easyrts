#pragma once

#include <string>
#include "spritesheet_info.h"

namespace EasyRTS
{
   class ResourceInfo
   {
   public:
      std::string mResourceName;
      int mAmount;
      SpritesheetInfo* mSpritesheet;
      std::string mThumbnail;
   };
}
