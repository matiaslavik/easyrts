#pragma once

#include <string>
#include <unordered_map>
#include <glm/vec2.hpp>

namespace EasyRTS
{
   /*
   * Information about an animation (usually a single row) inside a spritesheet.
   **/
   struct SpriteAnimInfo
   {
   public:
      /* Position in sprite sheet */
      glm::tvec2<int> mSpritesheetPos;
      /* Number of frames (columns) in animation */
      int mFrameCount = 1;
      /* Frames per second */
      float mFPS = 1.0f;
   };

   /*
   * Information about a sprite sheet, containing animations.
   **/
   class SpritesheetInfo
   {
   public:
      /* path/ID of sprite sheet file. */
      std::string mSpriteSheet;

      /* The width and height of a single sprite in the sprite sheet. */
      glm::tvec2<int> mSpriteSize;

      /* Maps animation state to animation (animation state = "walk", "idle", etc.) */
      std::unordered_map<std::string, SpriteAnimInfo> mAnimations;

      /* Number of tiles occupied by a sprite. */
      glm::tvec2<int> mOccupyTileCount;
   };
}
