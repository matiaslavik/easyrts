#include "scene.h"
#include "unit_conversion.h"
#include "asset_manager.h"
#include <algorithm>
#include <execution>
#include "path_query.h"
#include "glm/detail/func_geometric.hpp"

namespace EasyRTS
{
   Scene::Scene(AssetManager* assetManager)
   {
      mAssetManager = assetManager;
   }

   std::shared_ptr<Building> Scene::addBuilding(BuildingInfo* buildingInfo)
   {
      std::shared_ptr<Building> building = std::make_shared<Building>(buildingInfo, mAssetManager);
      mEntities.push_back(building);
      return building;
   }

   std::shared_ptr<Resource> Scene::addResource(ResourceInfo* resourceInfo)
   {
      std::shared_ptr<Resource> resource = std::make_shared<Resource>(resourceInfo, mAssetManager);
      mEntities.push_back(resource);
      return resource;
   }

   std::shared_ptr<Unit> Scene::addUnit(UnitInfo* unitInfo)
   {
      std::shared_ptr<Unit> unit = std::make_shared<Unit>(unitInfo, mAssetManager);
      mEntities.push_back(unit);
      mUnits.push_back(unit);
      return unit;
   }

   void Scene::setTilemap(sf::Texture* texture)
   {
      mTileTexture = texture;
      crateGroundSprites();
   }

   void Scene::setMapSize(const int width, const int height)
   {
      mMapDimension = glm::ivec2(width, height);
      mGroundTiles.clear();
      mGroundTiles.resize(width * height);
   }

   void Scene::setGroundTile(const int tileX, const int tileY, const int spriteX, const int spriteY)
   {
      mGroundTiles[tileX + tileY * mMapDimension.x] = glm::ivec2(spriteX, spriteY);
      crateGroundSprites(); // TODO: Just update the tile that changed
   }

   void Scene::tickScene(float deltaTime)
   {

   }

   void Scene::crateGroundSprites()
   {
      mMapSprites.clear();
      mMapSprites.reserve(mMapDimension.x * mMapDimension.y);

      for (int y = 0; y < mMapDimension.y; ++y)
      {
         for (int x = 0; x < mMapDimension.x; ++x)
         {
            sf::Sprite sprite;
            sprite.setTexture(*mTileTexture);
            sprite.setTextureRect(sf::IntRect(0, 0, 64, 32));

            glm::vec2 carPos(x * 32, y * 32);
            glm::vec2 iso = UnitConversion::cartesianToIsometric(carPos);

            sprite.setPosition(iso.x, iso.y);

            mMapSprites.push_back(sprite);
         }
      }
   }

   EntityRef Scene::getEntityAtPosition(const float x, const float y)
   {
      // TODO: optimise lookup (use a grid/quadtree or something)

      // Look for entities in grid cell
      auto it = std::find_if(mEntities.begin(), mEntities.end(), [&](EntityRef& entityRef) { return glm::ivec2(entityRef.getEntity()->mCartesianPos) == glm::ivec2(x, y); });

      if (it == mEntities.end())
      {
         // Look for Units
         const glm::vec2 mouseCartPos = glm::vec2(x, y);
         auto itUnit = std::find_if(mUnits.begin(), mUnits.end(), [&](std::shared_ptr<Unit> unit) { return glm::length((unit->mCartesianPos + glm::vec2(0.5f, 0.5f)) - mouseCartPos) < 1.0f; });
         if (itUnit != mUnits.end())
            return EntityRef(*itUnit);
         else
            return EntityRef(nullptr);
      }
      else
         return *it;
   }
}
