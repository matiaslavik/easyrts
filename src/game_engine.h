#pragma once

#include "asset_manager.h"
#include "time_manager.h"
#include <vector>
#include <set>
#include "scene.h"

namespace sf
{
   class RenderWindow;
   class View;
}

namespace EasyRTS
{
   class ScriptManager;
   class WidgetRenderer;
   class SceneRenderer;
   class Scene;
   class Unit;
   class Building;
   class Resource;
   class InputManager;
   class PathQueryManager;
   class UnitMovementManager;

   class GameEngine
   {
   private:
      sf::RenderWindow* mRenderWindow;
      sf::View* mView;
      AssetManager* mAssetManager;
      TimeManager* mTimeManager;
      ScriptManager* mScriptManager;
      InputManager* mInputManager;
      PathQueryManager* mPathQueryManager;
      UnitMovementManager* mUnitMovementManager;

      Scene* mScene;
      SceneRenderer* mSceneRenderer;

      WidgetRenderer* mWidgetRenderer;

      void handeMouseScroll();

   public:
      void initialise();
      void update();

      glm::vec2 screenToWorld(glm::ivec2 screenPos) const;

      inline AssetManager* getAssetManager() { return mAssetManager; }
      inline Scene* getScene() { return mScene; }
      inline TimeManager* getTimeManager() { return mTimeManager; }
      inline WidgetRenderer* getWidgetRenderer() { return mWidgetRenderer; }
      inline InputManager* getInputManager() { return mInputManager; }
      inline PathQueryManager* getPathQueryManager() { return mPathQueryManager; }
      inline UnitMovementManager* getUnitMovementManager() { return mUnitMovementManager; }
   };
}
