#pragma once
#include "glm/vec2.hpp"
#include "glm/vec4.hpp"
#include "SFML/Graphics.hpp"

namespace EasyRTS
{
   class TileHighlighter
   {
   public:
      sf::Sprite* mSprite = nullptr;

      /* Tile to highlight. */
      glm::ivec2 mTile;

      /* Size (dimension or number of tiles). */
      glm::ivec2 mDimension = glm::ivec2(1, 1);

      /* Highlight colour. */
      glm::vec4 mColour = glm::vec4(1.0f, 0.0f, 0.0f, 0.5f);
   };
}
